terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "1.29.2"
    }
  }
}

provider "linode" {
  token = var.token
}

resource "linode_instance" "terraform-web"{
        image = "linode/ubuntu18.04"
        label = "Terraform-Web-Example"
        group = "Terraform"
        region = var.region
        type = var.type
        authorized_keys = [ var.authorized_keys ]
        root_pass = var.root_pass
}