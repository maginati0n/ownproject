variable "token" {}
variable "authorized_keys" {}
variable "root_pass" {}
variable "type"{}
variable "region" {
  default = "ap-south"
}